import NoteIcon from '@material-ui/icons/Note';

import { SectionShow } from './SectionShow';
import { SectionCreate } from './SectionCreate';
import { SectionEdit } from './SectionEdit';

export default {
    show: SectionShow,
    create: SectionCreate,
    edit: SectionEdit, 
    icon: NoteIcon
};