<?php
/**
 * Created by PhpStorm.
 * User: benoitdelboe
 * Date: 22/10/2019
 * Time: 16:44
 */

namespace App\Solidary\Service;

use App\Solidary\Entity\Solidary;

class SolidaryManager
{
    /**
     * Create Solidary Ask
     *
     * @param Solidary $solidary
     * @return Solidary
     */
    public function createSolidary(Solidary $solidary)
    {
        // todo: add logic
        return $solidary;
    }
}
