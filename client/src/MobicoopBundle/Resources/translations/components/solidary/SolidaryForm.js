export default {
  fr: {
    ui: {
      buttons: {
        validate: {
          label: "Valider",
          route: "/solidarite/demande"
        },
        cancel: {
          label: "Annuler"
        }
      }
    },
    yearOfBirth: {
      label: "Année de naissance",
      placeholder: "Année de naissance"
    },
    hasRSA: {
      label: "Bénéficiaire du RSA",
      placeholder: "Bénéficière du RSA"
    },
    subject: {
      label: "Objet du déplacement",
      placeholder: "Objet du déplacement"
    },
    structure: {
      label: "Structure accompagnante",
      placeholder: "Structure accompagnante"
    },
    other: {
      label: "Autre",
      placeholder: "Autre"
    },
    success: "Votre demande de coup de pouce a bien été envoyée !"
  },
  en: {
    ui: {
      buttons: {
        validate: {
          label: "Validate",
          route: "/solidary/ask"
        },
        cancel: {
          label: "Cancel"
        }
      }
    },
    yearOfBirth: {
      label: "Year of birth",
      placeholder: "Year of birth"
    },
    hasRSA: {
      label: "",
      placeholder: ""
    },
    subject: {
      label: "",
      placeholder: ""
    },
    structure: {
      label: "",
      placeholder: ""
    },
    other: {
      label: "Other",
      placeholder: "Other"
    }
  }
}