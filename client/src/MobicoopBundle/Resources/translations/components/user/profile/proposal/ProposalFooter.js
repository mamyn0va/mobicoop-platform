export default {
  fr: {
    seat: {
      singular: "place",
      plural: "places"
    },
    potentialCarpooler: {
      singular: "covoitureur potentiel",
      plural: "covoitureurs potentiels"
    },
    urlResult:"/covoiturage/annonce/{id}/resultats"
  },
  en: {
    seat: {
      singular: "seat",
      plural: "seats"
    },
    potentialCarpooler: {
      singular: "potential carpooler",
      plural: "potentials carpoolers"
    },
    urlResult:"/carpool/ad/{id}/results"
  }
}